<?php

namespace Ynov\Mock\Test;

use PHPUnit\Framework\TestCase;
use Ynov\Mock\CartController;
use Ynov\Mock\AddressInfoInterface;
use Ynov\Mock\CardInterface;
use Ynov\Mock\CartItemInterface;
use Ynov\Mock\CartServiceInterface;
use Ynov\Mock\PaymentServiceInterface;
use Ynov\Mock\ShipmentServiceInterface;

class CartControllerTest extends TestCase
{
    private $cartServiceMock;
    private $paymentServiceMock;
    private $shipmentServiceMock;
    private $cardMock;
    private $addressInfoMock;

    /**
     * @before
     */
    public function setUp(): void
    {
        $this->cartServiceMock = $this->createMock(CartServiceInterface::class);
        $this->paymentServiceMock = $this->createMock(PaymentServiceInterface::class);
        $this->shipmentServiceMock = $this->createMock(ShipmentServiceInterface::class);
        $this->cardMock = $this->createMock(CardInterface::class);
        $this->addressInfoMock = $this->createMock(AddressInfoInterface::class);
        $cartItemMock = $this->createMock(CartItemInterface::class);

        $cartItemMock->method('getPrice')->willReturn(10.0);
        $this->cartServiceMock->method('items')->willReturn([$cartItemMock]);
    }

    public function testShouldReturnCharged(): void
    {
        $controller = new CartController($this->cartServiceMock, $this->paymentServiceMock, $this->shipmentServiceMock);
        $this->paymentServiceMock->method('charge')->willReturn(true);

        $result = $controller->checkOut($this->cardMock, $this->addressInfoMock);

        self::assertEquals("charged", $result);
    }

    public function testShouldReturnNotCharged(): void
    {
        $controller = new CartController($this->cartServiceMock, $this->paymentServiceMock, $this->shipmentServiceMock);

        $result = $controller->checkOut($this->cardMock, $this->addressInfoMock);

        $this->shipmentServiceMock->expects($this->never())->method("ship");
        self::assertEquals("not charged", $result);
    }
}
