<?php

namespace Ynov\Mock;

interface CartItemInterface
{
    public function getProductId(): string;
    public function setProductId(): string;

    public function getQuantity(): int;
    public function setQuantity(): int;

    public function getPrice(): float;
    public function setPrice(): float;
}
