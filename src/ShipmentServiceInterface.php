<?php

namespace Ynov\Mock;

interface ShipmentServiceInterface
{
    public function ship($info, $items): void;
}
