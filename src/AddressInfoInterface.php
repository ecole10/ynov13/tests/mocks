<?php

namespace Ynov\Mock;

interface AddressInfoInterface
{
    public function getStreet(): string;
    public function setStreet(): string;

    public function getAddress(): string;
    public function setAddress(): string;

    public function getCity(): string;
    public function setCity(): string;

    public function getPostalCode(): string;
    public function setPostalCode(): string;

    public function getPhoneNumber(): string;
    public function setPhoneNumber(): string;
}
