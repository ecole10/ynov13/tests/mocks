<?php

namespace Ynov\Mock;

interface CartServiceInterface
{
    public function total(): float;
    public function items();
}
