<?php

namespace Ynov\Mock;

interface PaymentServiceInterface
{
    public function charge($total, $card): bool;
}
