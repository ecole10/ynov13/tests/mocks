<?php

namespace Ynov\Mock;

use DateTime;

interface CardInterface
{
    public function getCardNumber(): string;
    public function setCardNumber(): string;

    public function getName(): string;
    public function setName(): string;

    public function getValidTo(): DateTime;
    public function setValidTo(): DateTime;
}
