<?php

namespace Ynov\Mock;

class CartController
{
    private $cartServiceInterface;
    private $paymentServiceInterface;
    private $shipmentServiceInterface;

    public function __construct
    (
        CartServiceInterface $cartServiceInterface,
        PaymentServiceInterface $paymentServiceInterface,
        ShipmentServiceInterface $shipmentServiceInterface
    )
    {
        $this->cartServiceInterface = $cartServiceInterface;
        $this->paymentServiceInterface = $paymentServiceInterface;
        $this->shipmentServiceInterface = $shipmentServiceInterface;
    }

    public function checkOut($card, $addressInfo): string
    {
        $result = $this->paymentServiceInterface->charge($this->cartServiceInterface->total(), $card);

        if ($result) {
            $this->shipmentServiceInterface->ship($addressInfo, $this->cartServiceInterface->items());
            return "charged";
        } else {
            return "not charged";
        }
    }
}
